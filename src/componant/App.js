import './App.css';
import React from "react";
import {Route, Switch} from 'react-router-dom';
import Home from "./page/Home";
import './css/style.css'
import Projects from "./page/Projects";
import Tasks from "./page/Tasks";
import Users from "./page/Users";
import NotFoundPage from "./page/Errors/NotFound";
import AddNewProject from "./page/Projects/AddNewProject";
import AddNewTask from "./page/Tasks/AddNewTask";
import AddNewUser from "./page/Users/AddNewUser";


class App extends React.Component {
    state = {
        projects: [
            {
                id: '1',
                projectName: 'First Project',
                creator: 'Ahmed',
                startTime: '10-03-2021',
                endTime: '12-03-2021',
            },
            {
                id: '2',
                projectName: 'Second Project',
                creator: 'Mohamed',
                startTime: '10-03-2021',
                endTime: '12-03-2021',
            },
            {
                id: '3',
                projectName: 'Third Project',
                creator: 'Ali',
                startTime: '10-03-2021',
                endTime: '12-03-2021',
            },
        ],
        tasks: [
            {
                id: '1',
                projectName: 'First Project',
                taskTitle: 'First Task',
                description: "This is the first task and it's part of First Project",
                assignTo: 'Hassan',
                reportTo: 'Ahmed',
                currentStatus:'Todo',
                status: [
                    {
                        todo: 'Todo',
                        done: 'Done',
                        inprogress: 'In Progress',
                    }
                ],
            },
            {
                id: '2',
                projectName: 'First Project',
                taskTitle: 'Second Task',
                description: "This is the Second task and it's part of First Project",
                assignTo: 'Hassan',
                reportTo: 'Ahmed',
                currentStatus:'Todo',
                status: [
                    {
                        todo: 'Todo',
                        done: 'Done',
                        inprogress: 'In Progress',
                    }
                ],
            },
            {
                id: '3',
                projectName: 'Second Project',
                taskTitle: 'First Task',
                description: "This is the first task and it's part of Second Project",
                assignTo: 'Omar',
                reportTo: 'Mohamed',
                currentStatus:'Todo',
                status: [
                    {
                        todo: 'Todo',
                        done: 'Done',
                        inprogress: 'In Progress',
                    }
                ],
            },
            {
                id: '4',
                projectName: 'Third Project',
                taskTitle: 'First Task',
                description: "This is the first task and it's part of Third Project",
                assignTo: 'Saaid',
                reportTo: 'Ali',
                currentStatus:'Todo',
                status: [
                    {
                        todo: 'Todo',
                        done: 'Done',
                        inprogress: 'In Progress',
                    }
                ],
            },
        ],
        users: [
            {
                id: '1',
                userName: 'Ahmed',
                userTitle: 'Manager',
            },
            {
                id: '2',
                userName: 'Ali',
                userTitle: 'Manager',
            },
            {
                id: '3',
                userName: 'Mohamed',
                userTitle: 'Manager',
            },
            {
                id: '4',
                userName: 'Saaid',
                userTitle: 'Developer',
            },
            {
                id: '5',
                userName: 'Omar',
                userTitle: 'Developer',
            },
            {
                id: '6',
                userName: 'Hassan',
                userTitle: 'Developer',
            },
        ],
        newproject:
            {
                id: '',
                projectName: '',
                creator: '',
                startTime: '',
                endTime: '',
            },

        newUser:
            {
                id: '',
                userName: '',
                userTitle: '',
            },
        newTask:
            {
                id: '',
                projectName: '',
                taskTitle: '',
                description: "",
                assignTo: '',
                reportTo: '',
                currentStatus:'',
                status: [
                    {
                        todo: 'Todo',
                        done: 'Done',
                        inprogress: 'In Progress',
                    }
                ]
            },

    }


    getNewproject = e => {

        const newproject = this.state.newproject;
        newproject[e.target.id] = e.target.value;
        this.setState({newproject})
    }

    getNewUser = e => {
        console.log(e.target.id)
        const newUser = this.state.newUser;
        newUser[e.target.id] = e.target.value;
        this.setState({newUser})
    }

    getNewTask = e => {
        console.log(e.target.id)
        const newTask = this.state.newTask;
        newTask[e.target.id] = e.target.value;
        console.log(newTask[e.target.id])
        this.setState({newTask})
    }


    addNew = (e) =>{
        e.preventDefault();

        if(e.target.dataset.btname === 'btnproject'){
            const newproject = this.state.newproject;
            if (this.state.projects.length === 0) {  // for the Projects Form
                newproject.id = 1;
            } else {
                newproject.id = parseInt(this.state.projects[this.state.projects.length - 1].id) + 1;
            }
            const projects = [...this.state.projects,newproject];
            const emptyProject = {
                id: '',
                projectName: '',
                creator: '',
                startTime: '',
                endTime: '',
            };

            this.setState({projects,newproject:emptyProject});
            console.log('insider..' + newproject)

        }else if(e.target.dataset.btname === 'btnuser'){  // for the Users Form
            const newUser = this.state.newUser;
            if (this.state.users.length === 0) {
                newUser.id = 1;
            } else {
                newUser.id = parseInt(this.state.users[this.state.users.length - 1].id) + 1;
            }
            const users = [...this.state.users,newUser];
            const emptyUser = {
                id: '',
                userName: '',
                userTitle: '',
            };

            this.setState({users,newUser:emptyUser});

        }else if(e.target.dataset.btname === 'btntask'){
            const newTask = this.state.newTask;
            if (this.state.tasks.length === 0) {
                newTask.id = 1;
                newTask.currentStatus = 'ToDo'
            } else {
                newTask.id = parseInt(this.state.tasks[this.state.tasks.length - 1].id) + 1;
                newTask.currentStatus = 'ToDo'
            }
            const tasks = [...this.state.tasks,newTask];
            const emptyTask = {
                    id: '',
                    projectName: '',
                    taskTitle: '',
                    description: "",
                    assignTo: '',
                    reportTo: '',
                    currentStatus:'',
                    status: [
                        {
                            todo: 'Todo',
                            done: 'Done',
                            inprogress: 'In Progress',
                        }
                    ]
                }

            this.setState({tasks,newTask:emptyTask});
        }


    }

    render() {
        return (
            <div className="container">
                <Switch>
                    {/*home page*/}
                    <Route path="/" render={props => <Home
                        {...props} projects={this.state.projects}
                        tasks={this.state.tasks} users={this.state.users}/>} exact/>
                    {/*project list page*/}
                    <Route path="/projects" render={props =>
                        <Projects {...props} projects={this.state.projects}/>}/>
                    {/*Task list page*/}
                    <Route path="/tasks" render={props =>
                        <Tasks {...props} tasks={this.state.tasks}/>}/>
                    {/*user list page*/}
                    <Route path="/users" render={props => <Users {...props} users={this.state.users}/>}/>
                    {/*project form page*/}
                    <Route path="/addproject" render={props =>
                        <AddNewProject {...props} addproject={this.addNew}
                                       getNewproject={this.getNewproject} newproject={this.state.newproject}/>}/>
                    {/*task form page*/}

                    <Route path="/addtask" render={props => <AddNewTask
                        {...props} getNewTask={this.getNewTask} addtask={this.addNew}
                    newtask={this.state.newTask} users={this.state.users} projects={this.state.projects}/>}/>

                    {/*user form page*/}
                    <Route path="/adduser" render={props => <AddNewUser
                        {...props} newuser={this.state.newUser}  adduser={this.addNew}
                        getNewUser={this.getNewUser}/>}/>
                    {/*home page*/}
                    <Route path="*" component={NotFoundPage}/>

                </Switch>
            </div>
        );
    }
}

export default App;
