

const ProjectList = ({projects}) =>(
    <div className="projects-table">
        <h3 className="title">Projects Table</h3>
        <table className="table">
            <thead className="thead-dark">
            <tr>
                <th scope="col">Project Name</th>
                <th scope="col">Creator</th>
                <th scope="col">StartTime</th>
                <th scope="col">EndTime</th>
                <th scope="col">Update</th>
                <th scope="col">Show</th>
            </tr>
            </thead>
            <tbody>
            {projects.map(project =>
                <tr key={`project-${project.id}`}>
                    <td>{project.projectName}</td>
                    <td>{project.creator}</td>
                    <td>{project.startTime}</td>
                    <td>{project.endTime}</td>
                    <td >
                        <button className="btn btn-success">Update</button>
                    </td>
                    <td>
                        <button className="btn btn-secondary">Show</button>
                    </td>
                </tr>
            )}


            </tbody>
        </table>
    </div>
)

export default ProjectList;