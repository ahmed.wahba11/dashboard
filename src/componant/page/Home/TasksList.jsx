const TaskList = ({tasks}) =>(
    <div className="tasks-table">
        <h3 className="title">Tasks Table</h3>
        <table className="table">
            <thead className="thead-dark">
            <tr>
                <th scope="col">project Name</th>
                <th scope="col">task Title</th>
                <th scope="col">description</th>
                <th scope="col">assign To</th>
                <th scope="col">report To</th>
                <th scope="col">status</th>
            </tr>
            </thead>
            <tbody>

            {tasks.map(task =>
                <tr key={`task-${task.id}`}>
                    <td>{task.taskTitle}</td>
                    <td>{task.projectName}</td>
                    <td>{task.description}</td>
                    <td>{task.assignTo}</td>
                    <td>{task.reportTo}</td>
                    <td>
                        <select>
                            <option value={task.status.todo}>TODO</option>
                            <option value={task.status.done}>DONE</option>
                            <option value={task.status.inprogress}>In Progress</option>
                        </select>
                    </td>
                </tr>
            )}
            </tbody>
        </table>
    </div>
)

export default TaskList;