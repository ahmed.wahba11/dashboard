const UserList = ({users}) =>(
    <div className="users-table">
        <h3 className="title">Users Table</h3>
        <table className="table">
            <thead className="thead-dark">
            <tr>
                <th scope="col">user name</th>
                <th scope="col">title</th>
            </tr>
            </thead>
            <tbody>

            {users.map(user =>
                <tr key={`user-${user.id}`}>
                    <td>{user.userName}</td>
                    <td>{user.userTitle}</td>
                </tr>
            )}
            </tbody>
        </table>
    </div>
)

export default UserList;