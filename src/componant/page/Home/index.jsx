import React from 'react';
import Navbar from "../navbar";
import ProjectList from "./ProjectsList";
import TaskList from "./TasksList";
import UserList from "./UsersList";


const Home = ({projects, tasks, users}) => (
    <div>
        <Navbar/>
        <div className="home">
            <ProjectList projects={projects}/>
            <TaskList tasks={tasks}/>
            <UserList users={users}/>
        </div>
    </div>
)

export default Home;