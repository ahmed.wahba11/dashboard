import Navbar from "../navbar";
import {Link} from "react-router-dom";

const NewProject = ({newproject,addproject,getNewproject,history}) =>{


    // console.log(props.history.push('/'))
    return (

        <div className="add-new-project">
            <Navbar/>
            <h3 className="title">add new project</h3>
            <form data-btname="btnproject" className='form-projects'
                  onSubmit={(e) =>{
                addproject(e);
                history.push('/projects');
            }}>
                <div className="form-group row">
                    <label htmlFor="projectName" className="col-sm-2 col-form-label">project Name</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" id="projectName" onChange={getNewproject}
                               value={newproject.projectName}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="creator" className="col-sm-2 col-form-label">creator</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" id="creator" onChange={getNewproject}
                               value={newproject.creator}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="startTime" className="col-sm-2 col-form-label">start Date</label>
                    <div className="col-sm-10">
                        <input type="date" className="form-control" id="startTime"
                               onChange={getNewproject} value={newproject.startTime}/>

                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="endTime" className="col-sm-2 col-form-label">end Date</label>
                    <div className="col-sm-10">
                        <input type="date" className="form-control" id="endTime"
                               onChange={getNewproject} value={newproject.endTime}/>
                    </div>
                </div>
                <button  className="btn btn-success" style={{marginRight:'20px'}}>Submit</button>
                <Link to="/projects" className="btn btn-dark"> Back </Link>
            </form>
        </div>
    );
}

export default NewProject;