import React from 'react';
import Navbar from "../navbar";
import ProjectList from "../Home/ProjectsList";
import {Link} from "react-router-dom";

const Projects = ({projects}) =>(
    <div className="home">
        <Navbar/>
        <ProjectList  projects={projects}/>
        <Link to="/addproject" className="btn btn-primary">+ Add New Project</Link>
    </div>
)

export default Projects;