import Navbar from "../navbar";


const NewTask = ({getNewTask,addtask,newtask,history,users,projects}) =>(

    <div>
        <Navbar/>
        <h3 className="title">add new project</h3>
        <form data-btname="btntask" className="form-projects" onSubmit={e =>{
            addtask(e);
            history.push('/tasks')
        }}>
            <div className="form-row">
                <div className="col-md-6 mb-3">
                    <label htmlFor="projectName">project Name</label>
                    <select className="custom-select" >
                        {projects.map(project =>

                            <option key={`pro-${newtask.id}`} id="projectName" onChange={getNewTask} value={project.projectName}>
                                {project.projectName}</option>
                        )}
                    </select>
                </div>
                <div className="col-md-6 mb-3">
                    <label htmlFor="taskTitle">Task name</label>
                    <input type="text" className="form-control" id="taskTitle"
                          onChange={getNewTask} value={newtask.taskTitle}/>
                </div>
            </div>
            <div className="form-row">
                <div className="col-md-12 mb-3">
                    <label htmlFor="description">description</label>
                    <textarea type="text" className="form-control" id="description"
                    onChange={getNewTask} value={newtask.description}/>
                </div>
                <div className="col-md-6 mb-3">
                    <label htmlFor="userTitle">Assign to</label>
                    <select className="custom-select" id="userTitle">
                        {users.map(user =>{
                            if(user.userTitle === 'Developer'){
                                return(
                                    <option key={`user-${user.id}`} onChange={getNewTask}
                                            value={newtask.assignTo}>{user.userName}</option>
                                )}
                        }
                        )}
                    </select>
                </div>
                <div className="col-md-6 mb-3">
                    <label htmlFor="userTitle">Report to</label>
                    <select className="custom-select" id="userTitle">
                        {users.map(user =>{
                                if(user.userTitle === 'Manager'){
                                    return(
                                        <option key={`user-${user.id}`} onChange={getNewTask}
                                                value={newtask.reportTo}>{user.userName}</option>
                                    )}
                            }
                        )}
                    </select>
                </div>
            </div>

            <button className="btn btn-primary" type="submit">Submit form</button>
        </form>

    </div>
);

export default NewTask;