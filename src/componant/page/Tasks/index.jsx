import React from 'react';
import Navbar from "../navbar";
import {Link} from "react-router-dom";
import TaskList from "../Home/TasksList";

const Tasks = ({tasks}) =>(
    <div className="home">
        <Navbar/>
        <TaskList tasks={tasks}/>
        <Link className="btn btn-primary" to='/addtask'>+ Add Task</Link>
    </div>
)

export default Tasks;