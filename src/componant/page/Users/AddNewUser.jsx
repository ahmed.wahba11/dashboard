import Navbar from "../navbar";
import {Link} from "react-router-dom";

const NewUser = ({newuser,adduser,getNewUser,history}) =>(

    <div>
        <Navbar/>
        <h3 className="title">add new project</h3>
        <form data-btname="btnuser" className='form-projects' onSubmit=
            {(e) =>{
                adduser(e);
                history.push('/users');
            }

        }>
            <div className="form-group row">
                <label htmlFor="userName" className="col-sm-2 col-form-label">user Name</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="userName" onChange={getNewUser}
                           value={newuser.userName}/>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="userTitle" className="col-sm-2 col-form-label">title</label>
                <div className="col-sm-10">
                    <input type="text" className="form-control" id="userTitle" onChange={getNewUser}
                           value={newuser.userTitle}/>
                </div>
            </div>

            <button  className="btn btn-success" style={{marginRight:'20px'}}>Submit</button>
            <Link to="/users" className="btn btn-dark"> Back </Link>
        </form>
    </div>
);

export default NewUser;