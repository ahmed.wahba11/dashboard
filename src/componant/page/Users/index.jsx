import React from 'react';
import Navbar from "../navbar";
import UserList from "../Home/UsersList";
import {Link} from "react-router-dom";

const Users = ({users}) =>(
    <div className="home">
        <Navbar/>
        <UserList users={users}/>
        <Link to="/adduser" className="btn btn-primary">+ Add New User</Link>
    </div>
)

export default Users;