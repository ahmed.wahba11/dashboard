import React from 'react';
import {NavLink} from "react-router-dom";


const Navbar = () => (
    <div className="main-navbar">
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/" exact>Home </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/projects">Projects</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/tasks">Tasks</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/users">Users</NavLink>
                    </li>
                </ul>
            </div>
        </nav>


    </div>
)

export default Navbar;